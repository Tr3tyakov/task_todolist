/**
 * Компонент, который получает
 * @param {boolean} hide - значение, позволяющее скрывать или показывать строку с добавлением файла
 * @param {objet} file - файл загрузки, который состоит из url, image,
 * @param {function} changeFile - функция для изменения обьекта файт file
 *
 */

const UploadFile = ({ hide, file, changeFile }) => {
  const visibility = hide ? file.image : false;

  return (
    <Box
      backgroundColor={theme.palette.blue.secondary}
      display="flex"
      justify={'space-between'}
      align="center"
      boradius="5px"
      p="5px"
      m="0 0 5px 0">
      {visibility ? (
        <Box>
          <Typography fontSize="16px" color="white" gutterButtom>
            File successfully upload
          </Typography>
        </Box>
      ) : (
        <Box display="flex" justify="space-evenly" flexDirection="column">
          <Typography fontSize="18px" fontWeight={600} color="white" gutterButtom>
            {file.image && file.url === '' ? 'Change File' : 'Add File'}
          </Typography>
          <Input type="file" value={file.url} onChange={(e) => changeFile(e)} />
        </Box>
      )}
      <Box p="5px" boradius="5px" width="100px" height="100px" overflow="hidden">
        <img className="upload" src={file.image ? file.image : camera} alt="upload image" />
      </Box>
    </Box>
  );
};

/**
 * Компонент, который отвечает за отрисовку таблицы и модального окна
 */
const Todo = () => {
  const [modal, setModal] = React.useState < boolean > false;

  /**
   * Функция, позволяющая открывать и закрывать модалку
   */
  const changeActiveModal = () => {
    setModal(!modal);
  };
  return (
    <>
      <Modal active={modal} changeActiveModal={changeActiveModal} />
      <Box width="100wh" height="100vh" display="flex" justify="center" align="center">
        <Box>
          <Box>
            <Typography fontSize="35px" fontWeight={600}>
              TODO List of your App
            </Typography>
            <Box display="flex" justify="space-between">
              <Box
                p="5px 5px"
                backgroundColor={theme.palette.yellow.primary}
                boradius="5px"
                width="max-content">
                <Typography fontSize="15px" fontWeight={500}>
                  Just DO IT now.
                </Typography>
              </Box>
              <Button onClick={() => changeActiveModal()}>Create new Tusk</Button>
            </Box>
            <Table />
          </Box>
        </Box>
      </Box>
    </>
  );
};

/**
 * Компонента конкретной задачи
 */

const CurrentTodo = () => {
  /**
   * С помощью хука useLocation получаем все необходимые элементы конкретной задачи
   */
  const location = useLocation();
  const { task, about, id, completionDate, createDate, file } = location.state;
  /**
   * Сохраняем данные в отдельном стейте, чтобы при изменении данных мы могли в дальнейшем выбрать стоил ли нам сохранять изменения
   */
  const [state, setState] =
    React.useState <
    IState >
    {
      locationTask: task,
      locationCompletionDate: completionDate,
      locationAbout: about,
      locationFile: {
        url: '',
        image: file.image,
      },
    };

  const dispatch = useAppDispatch();

  /**
   * Функция для изменения даты в стейте
   * @param {event} e
   * с помощью toLocaleDateString добиваемся нужного формата даты
   */
  const changeDate = (e) => {
    const newDate = new Date(e.currentTarget.value).toLocaleDateString('ru-RU');
    setState({ ...state, locationCompletionDate: newDate });
  };
  /**
   * Функция для изменения добавленного файла в стейте
   * @param {event} e
   * создаем blob с помощью  URL.createObjectURL(value) для просмотра изображений, которые мы загрузили
   */
  const changeFile = (event: any) => {
    if (event.target.files[0]) {
      const value = event.target.files[0];
      const blob = URL.createObjectURL(value);
      setState({ ...state, locationFile: { url: event.target.value, image: blob } });
    }
  };
  /**
   * Функция для изменения строчки about task в стейте
   * @param {event} e
   */
  const changeAbout = (e) => {
    setState({ ...state, locationAbout: e.currentTarget.value });
  };
  /**
   * Функция для cохранения всех изменений, запускается при клике на кнопку
   * @param {event} e
   */
  const saveChanges = () => {
    const newInfo = {
      task: state.locationTask,
      about: state.locationAbout,
      file: state.locationFile,
      completionDate: state.locationCompletionDate,
    };
    dispatch(changeCurrentRow({ id, newInfo }));
  };

  return (
    <Box width="100wh" height="100vh" display="flex" justify="center" align="center">
      <Box p="10px" backgroundColor="white" boradius="4px">
        <Box>
          <Typography gutterButtom>
            <Link className="text__decoration--disabled" to={'/'}>
              Todo List
            </Link>
            &nbsp;/&nbsp;
            <Typography color="black" component="span" className="todo__current">
              Current Todo
            </Typography>
          </Typography>
        </Box>
        <Typography gutterButtom fontSize="35px" fontWeight={600}>
          {task}
        </Typography>
        <Box>
          <textarea
            className="todo__textarea"
            value={state.locationAbout}
            onChange={(e) => changeAbout(e)}></textarea>
        </Box>
        <Box display="flex" gap="10px" flexDirection="column">
          <Box>
            <Typography gutterButtom color="black" fontSize="16px" fontWeight={600}>
              Creation Date
            </Typography>
          </Box>
          <Typography>{createDate}</Typography>
          <Box>
            <Typography gutterButtom color="black" fontSize="16px" fontWeight={600}>
              Completion Date
            </Typography>
          </Box>
          <Input
            className="date"
            type="date"
            value={createCorrectionFormatForInput(state.locationCompletionDate)}
            onChange={(e) => changeDate(e)}
          />
          <UploadFile file={state.locationFile} changeFile={changeFile} />
          <Box>
            <Link className="text__decoration--disabled" to={'/'}>
              <Button className="todo__current__button" onClick={saveChanges}>
                Save
              </Button>
            </Link>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

/**
 *Заголовки таблицы попадающие в <thead>
 */
const tableHeader = [
  '№',
  'Task',
  'Date of Creation',
  'Date of Completion',
  'Status ',
  'Edit',
  'Delete',
  'Details',
];
/**
 * Компонента отображающая таблицу задач
 *
 */
const Table = () => {
  const state = useAppSelector((state) => state);

  return (
    <Box
      m="10px 0"
      backgroundColor={theme.palette.white}
      boradius="4px"
      overflow="hidden"
      box-shadow="2px 2px 2px 2px rgba(0, 0, 0, 0.19)">
      <table width="100%" cellPadding="8px" cellSpacing="0px">
        <thead>
          <tr>
            {tableHeader.map((title, index) => (
              <td width={index === 1 ? '23%' : ''} key={title}>
                {title}
              </td>
            ))}
          </tr>
        </thead>
        <tbody>
          {state.map((element) => (
            <TableRow key={element.id} element={element} />
          ))}
        </tbody>
      </table>
    </Box>
  );
};

/**
 * Компонента отображения задачаи
 * @params {element} конкретная задача
 */
const TableRow = React.memo(({ element }) => {
  const isEdit = element.edit;
  const isSuccess = element.status;

  const dispatch = useAppDispatch();
  /**
   * Функция для изменения заголовка
   * @params {e} event
   */
  const changeTitle = (e) => {
    dispatch(changeTitleTask({ id: element.id, task: e.currentTarget.value }));
  };
  /**
   * Функция для изменения булевого поля edit по конкретному id
   */
  const changeRowEdit = () => {
    dispatch(changeEdit(element.id));
  };
  /**
   * Функция для удаления задачи по конкретному id
   */
  const deleteCurrentTask = () => {
    dispatch(removeTask(element.id));
  };
  /**
   * Функция для изменения статуса задачи по конкретному id
   */
  const changeStatus = () => {
    dispatch(changeStatusRow(element.id));
  };
  /**
   *  Функция, выясняющая просрочена ли задача
   * @params {new Date} - настоящее время
   * @params {element.completionData} - время до которого данная задача должна быть выполнена
   */
  const isTimeOut = checkTaskTime(new Date(), element.completionDate);

  return (
    <tr className={isTimeOut ? 'table__row--disabled' : ''}>
      <td>{element.id}</td>
      <td onDoubleClick={changeRowEdit}>
        {isEdit ? (
          <form onSubmit={changeRowEdit}>
            <Input
              fontSize="16px"
              fontFamily='"Montserrat", sans-serif'
              value={element.task}
              color="black"
              fullWidth
              variant="secondary"
              onChange={(e) => changeTitle(e)}
            />
          </form>
        ) : (
          element.task
        )}
      </td>
      <td>{element.createDate}</td>
      <td>{element.completionDate === null ? '' : element.completionDate}</td>
      <td>
        {isSuccess ? (
          <img onClick={changeStatus} className="table__img" src={check} alt="Выполнено" />
        ) : (
          <img onClick={changeStatus} className="table__img" src={cross} alt="Не выпоплено" />
        )}
      </td>
      <td>
        <img onClick={changeRowEdit} className="table__img" src={edit} alt="Редактирование" />
      </td>
      <td>
        <img
          onClick={() => deleteCurrentTask()}
          className="table__img"
          src={trash}
          alt="Редактирование"
        />
      </td>
      <td>
        <Link to={`/${element.id}`} state={element}>
          <img className="table__img" src={back} alt="Редактирование" />
        </Link>
      </td>
    </tr>
  );
});

/**
 *  Функция, выясняющая просрочена ли задача
 * @params {currentDate} - настоящее время
 * @params {completionData} - время до которого данная задача должна быть выполнена
 * если у задачи не выставлено время до которого она должна быть выполнена, то просто возвращаем false, чтобы на задачу не навешивался класс table__row--disabled
 */
export const checkTaskTime = (currentDate, completionDate) => {
  if (completionDate === null) return false;
  const currentDateTime = currentDate.getTime();
  const completionDateTime = new Date(createCorrectionFormatForInput(completionDate)).getTime();
  return currentDateTime > completionDateTime;
};

/**
 * Компонент - модального окно, где создаются новые задачи
 * @params {active} параметр, показывающий стоит ли отображать модальное окно
 * @params {changeActiveModal} - функция для открытия/закрытия модального окна
 */
const Modal = ({ active, changeActiveModal }) => {
  /**
   * cтейт для создание задачи
   */
  const [newTask, setNewTask] =
    React.useState <
    INewElement >
    {
      task: '',
      about: '',
      createDate: new Date().toLocaleDateString('ru-RU'),
      completionDate: null,
      status: false,
      edit: false,
      remove: false,
      file: {
        url: '',
        image: '',
      },
    };

  const dispatch = useAppDispatch();

  /**
   * Функция для создания заголовка
   * @params {e} event
   */
  const changeTitle = (e) => {
    setNewTask({ ...newTask, task: e.currentTarget.value });
  };

  /**
   * Функция для создания описания
   * @params {e} event
   */
  const changeAbout = (e) => {
    setNewTask({ ...newTask, about: e.currentTarget.value });
  };

  /**
   * Функция для создания даты, при которой данная задачи будет неактуальна
   * @params {e} event
   */
  const changeData = (e) => {
    setNewTask({
      ...newTask,
      completionDate: new Date(e.target.value).toLocaleDateString('ru-RU'),
    });
  };

  /**
   * Функция для создания задачи
   * отправляет задачу в хранилище dispatch(addTask(newTask));
   * закрывает модальное окно changeActiveModal();
   * и сбрасывает все описания задачи setNewTask()
   */
  const createNewTask = () => {
    dispatch(addTask(newTask));
    changeActiveModal();
    setNewTask({
      task: '',
      about: '',
      createDate: new Date().toLocaleDateString('ru-RU'),
      completionDate: null,
      status: false,
      edit: false,
      remove: false,
      file: {
        url: '',
        image: '',
      },
    });
  };

  /**
   * Функция для добавления картинки
   * @params {e} event
   */
  const changeFile = (event) => {
    if (event.target.files[0]) {
      const value = event.target.files[0];
      const blob = URL.createObjectURL(value);
      setNewTask({ ...newTask, file: { url: event.target.value, image: blob } });
    }
  };

  /**
   * Если поле active - истина, то renderim компоненту
   * @params {e} event
   */
  if (active) {
    return ReactDOM.createPortal(
      <Box
        position="absolute"
        top="0"
        left="0"
        right="0"
        bottom="0"
        height="100vh"
        display="flex"
        justify="center"
        align="center"
        backgroundColor="rgba(0, 0, 0, 0.50)"
        onClick={changeActiveModal}>
        <Box
          backgroundColor="white"
          width="60%"
          height="60%"
          p="20px"
          boradius="4px"
          onClick={(e) => e.stopPropagation()}
          display="flex"
          justify="space-between"
          flexDirection="column">
          <Box>
            <Typography gutterButtom fontSize="35px" fontWeight={600}>
              Create Your Todo
            </Typography>
            <Box m="0 0 8px 0">
              <Typography gutterButtom>Title</Typography>
              <Input
                onChange={(e) => changeTitle(e)}
                value={newTask.task}
                fontSize="16px"
                variant="primary"
                fullWidth></Input>
            </Box>
            <Box m="0 0 8px 0">
              <Typography gutterButtom>About</Typography>
              <textarea
                onChange={(e) => changeAbout(e)}
                value={newTask.about}
                className="textarea"></textarea>
            </Box>
            <Box m="0 0 8px 0">
              <Typography gutterButtom>Date before task completion</Typography>
              <Input
                type="date"
                className="date"
                onChange={(e) => changeData(e)}
                value={
                  newTask.completionDate === null
                    ? ''
                    : createCorrectionFormatForInput(newTask.completionDate)
                }
                fontSize="16px"
                variant="primary"></Input>
            </Box>
          </Box>
          <UploadFile file={newTask.file} changeFile={changeFile} />
          <Box>
            <Button
              className="modal__button"
              onClick={() => {
                createNewTask();
              }}>
              Create New Task
            </Button>
          </Box>
        </Box>
      </Box>,
      document.getElementById('root'),
    );
  }
};

/**
 * Функция, которая преобразует значение времени в нужный формат для input'a
 * @param {string} строка
 * @returns yyyy/mm/dd
 */
export const createCorrectionFormatForInput = (string) => {
  if (string === null) return '';
  return string.split('.').reverse().join('.').replace(/\./g, '-');
};
